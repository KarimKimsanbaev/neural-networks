package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {
    private static ArrayList<CheckField>  obrazs ;
    private static ArrayList<Label>  lT, lB ;
    final int n = 5; // количество образов
    final int N=2, M=6;
    private static Label l1, lRes, lS, lNer;
    private static Button button ;
    private static ArrayList<int[][]> obraz;
    private static int countNerous;

    @Override
    public void start(Stage primaryStage) throws Exception{

        setObraz();


        obrazs = new ArrayList<>();
        lT = new ArrayList<>();
        lB = new ArrayList<>();
        Pane root = new Pane();
        ART art = new ART(N,M);
        lNer = new Label("Нейроны");
        lNer.relocate(350, 5);
        primaryStage.setTitle("Кимсанбаев К.А. Вариант №3");
        primaryStage.setScene(new Scene(root, 1000, 500));
        primaryStage.show();


        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Образец №1",
                        "Образец №2",
                        "Образец №3",
                        "Образец №4",
                        "Образец №5"
                );
        ComboBox comboBox = new ComboBox(options);
        comboBox.relocate(100,10);



        CheckField letter = new CheckField(6, 2, 20, 20, 4);
        letter.relocate(20, 100);
        l1 = new Label("ОБРАЗЕЦ:");
        l1.relocate(5, 10);
        lRes = new Label();
        lRes.relocate(5, 200);
        lS = new Label();
        lS.relocate(20, 250);
        button = new Button("Считать");
        button.relocate(20, 30);
        comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            int number;
            for (number=0; newValue != options.get(number);number++);
            letter.setChecks(obraz.get(number));
        });
        countNerous = 0;
        comboBox.setValue(options.get(0));
        button.setOnAction(event -> {
            lRes.setText(art.raspoznovanie(letter.getChecksLine()));
            String str = new String();
            int max=0;
            for(int i=0; i<art.S.size();i++){
                if(i==0){
                    str+="Sн=";
                }
                else {
                    str += "S"+i+"=";
                }
                str += String.format("%.3f", art.S.get(i)) +" ";
                if(art.S.get(max)<art.S.get(i)){
                    max=i;
                }
            }
            lS.setText(str);
            if(countNerous != art.T.size()){ //добавляю новый нейрон на экран
                CheckField ob = new CheckField(6, 2, 20, 20, 4);
                ob.setChecks(art.T.get(countNerous));
                ob.relocate(300, 25+countNerous*100);


                obrazs.add(ob);
                str="T = ";
                String str1 = new String("B=");
                for(int i=0; i<N; i++){
                    for(int j=0; j<M; j++){
                        str+=art.T.get(countNerous)[i][j]+" ";
                        str1+=String.format("%.3f ",art.B.get(countNerous)[i][j]);
                    }
                }
                lT.add(new Label(str));
                lB.add(new Label(str1));
                lT.get(countNerous).relocate(450, 25+countNerous*100);
                lB.get(countNerous).relocate(450, 45+countNerous*100);
                root.getChildren().addAll(ob,lT.get(countNerous),lB.get(countNerous));
                countNerous++;
            }
            else{// ОБновляю данные нейронов на экране
                obrazs.get(max-1).setChecks(art.T.get(max-1));
                String str1 = new String("B=");
                str = "T=";
                for(int i=0; i<N; i++){
                    for(int j=0; j<M; j++){
                        str+=art.T.get(max-1)[i][j]+" ";
                        str1+=String.format("%.3f ",art.B.get(max-1)[i][j]);
                    }
                }
                lT.get(max-1).setText(str);
                lB.get(max-1).setText(str1);
            }
                });




        root.getChildren().addAll( letter , l1, lRes, button,comboBox, lS, lNer);
    }

    public void setObraz(){
        obraz = new ArrayList<>();
        int a[][] = {{1, 1, 0, 1, 1, 1}, {0, 1, 0, 1, 0, 0}};
        obraz.add(a);
        int b[][] = {{0, 1, 0, 1, 0, 0}, {1, 1, 1, 1, 1, 1}};
        obraz.add(b);
        int g[][] = {{1, 1, 0, 1, 1, 1}, {0, 1, 1, 1, 0, 0}};
        obraz.add(g);
        int w[][] = {{1, 1, 0, 1, 0, 1}, {0, 1, 0, 1, 0, 0}};
        obraz.add(w);
        int r[][] = {{0, 1, 0, 1, 0, 0}, {1, 1, 1, 1, 1, 0}};
        obraz.add(r);

    }


    public static void main(String[] args) {
        launch(args);
    }
}
