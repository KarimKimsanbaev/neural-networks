package sample;

import java.util.ArrayList;


public class ART {
    int N, M , L;
    public ArrayList<int[][]>  T;
    public ArrayList<double[][]>  B;
    public ArrayList<Double> S; // Слой распознования

    int count1(int [][] k){ // Подсчет 1 в образе
        int count =0;
        for(int i=0; i<N; i++){
            for(int j=0; j<M; j++){
                if(k[i][j] == 1){
                    count++;
                }
            }
        }
        return count;
    }

    double neRasp; // вес нераспределенного нейрона

    double po;

    ART(int N, int M){
        this.N = N;
        this.M = M;
        L=2;
        neRasp = (double) L/(L-1+N*M);
        po = 0.9;
        S = new ArrayList<>();
        T = new ArrayList<>();
        B = new ArrayList<>();
    }
    ART(){
        this.N = 3;
        this.M = 3;
        L=2;
        neRasp = L/(L-1+N*M);
        po = 0.9;
    }
    // Добавление нового нейрона
    public void addNeuros(int[][] obraz){
        double newB[][] = new double[N][M];
        double b;
        int  count=0; // b - вес, count - количество единиц в образе
        count = count1(obraz);
        /*for(int i=0; i<N; i++){
            for(int j=0; j<M; j++){
                if(obraz[i][j] == 1){
                    count++;
                }
            }
        }*/
        b = (double)L/(L-1+count);
        for(int i=0; i<N; i++){
            for(int j=0; j<M; j++){
                if(obraz[i][j] == 1){
                    newB[i][j] = b;
                }
                else {
                    newB[i][j] = 0;
                }
            }
        }
        T.add(obraz);
        B.add(newB);

    }

    // Распознование. Возращает Текст с результатом
    public String raspoznovanie (int[][] obraz) {

        S.clear();
        int C[][] = new int[N][M], count = 0;

        double s;
        String str = new String ();
        S.add(count1(obraz) * neRasp);

        // Подсчет S для остальный нейронов
        for (int k = 0; k < B.size(); k++) {
            s = 0;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    s += B.get(k)[i][j] * obraz[i][j];
                }
            }
            S.add(s);
        }

        int max = 0;
        // Поиск max S
        for (int i = 1; i < S.size(); i++) {
            if (S.get(max) < S.get(i)) {
                max = i;
            }
        }
        max--;
        count = 0;
        //если вобедил нераспределенный то вводим новый нейрон
        if (max == -1) {
            addNeuros(obraz);
            str = "Победил нераспределенный\nнейрон => вводим новый нейрон";
        } else { //иначе Сравниваем
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    C[i][j] = obraz[i][j] * T.get(max)[i][j];
                    if (C[i][j] == obraz[i][j])
                        count++;
                }
            }
            double p = (double) count / (N * M);
            if (p < po) {
                str = "Победил нейрон № " + (max+1)+ " \nр = "+String.format("%.3f", p) + " < "+ po+ "=> вводим новый нейрон";
                addNeuros(obraz);
            } else { // Переобучение
                str = "Победил нейрон № " + (max+1)+ " \nр = "+ String.format("%.3f", p) + " > "+ po+ "=> переобучаем его";
                double b = (double) L / (L - 1 + count1(C));
                for (int i = 0; i < N; i++)  {
                    for (int j = 0; j < M; j++) {
                        T.get(max)[i][j] = C[i][j];
                        if (C[i][j] == 1) {
                            B.get(max)[i][j] = b;
                        } else {
                            B.get(max)[i][j] = 0;
                        }
                    }
                }


            }
        }
        return str;
    }
}
