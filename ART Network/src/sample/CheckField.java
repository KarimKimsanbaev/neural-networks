package sample;


import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;

public class CheckField extends Pane {
    private CheckBox checkBoxes[][];
    private int w, h;

    public CheckField(int w, int h, int singleW, int singleH, int padding) {
        this.w = w;
        this.h = h;

        this.getStylesheets().add(getClass().getResource("checkbox.css").toExternalForm());

        checkBoxes = new CheckBox[h][w];

        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++) {
                checkBoxes[i][j] = new CheckBox();
                checkBoxes[i][j].setPrefSize(singleW, singleH);
                checkBoxes[i][j].setLayoutY(i*(singleH+padding));
                checkBoxes[i][j].setLayoutX(j*(singleW+padding));

                this.getChildren().add(checkBoxes[i][j]);
            }

    }

    public int[][] getChecks() {
        int result[][] = new int[w][h];

        for (int i = 0; i < w; i++)
            for (int j = 0; j < h; j++)
                result[i][j] = (checkBoxes[i][j].isSelected())?1:0;

        return result;
    }

    public int[][] getChecksLine() {
        int result[][] = new int[h][w];

        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
                result[i][j] = (checkBoxes[i][j].isSelected()) ? 1 : 0;

        return result;
    }

    public void setChecks(int array[][]) {
        for (int i = 0; i < h; i++)
            for (int j = 0; j < w; j++)
                checkBoxes[i][j].setSelected(array[i][j] == 1);
    }
}
